# Development Production Line Docker Demo

## Project Setup

### Git

First create an empty [![Git](./documentation/icons/git.png "Git"){height=16px} Git](https://git-scm.com) project in `%PROJECT_HOME%`:

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
cd /D %PROJECT_HOME%
git init development-production-line-docker-demo
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PowerShell only:

```
cd $env:PROJECT_HOME
git init development-production-line-docker-demo
```

![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash only:

```
cd $PROJECT_HOME
git init development-production-line-docker-demo
```

Configure the [.gitignore](https://gitlab.com/freedumbytes/setup/-/raw/master/.gitignore) and [.gitattributes](https://gitlab.com/freedumbytes/setup/-/raw/master/.gitattributes).

Start this [![Markdown Mark](./documentation/icons/markdown-mark-outline.svg "Markdown Mark"){height=16px}](https://github.com/dcurtis/markdown-mark) `README.md`.

[![Markdown](./documentation/icons/markdown-mark-outline.svg "Markdown"){height=16px} Markdown](https://daringfireball.net/projects/markdown) is a lightweight markup language for creating formatted text using a plain-text editor.

 * [![GitLab](./documentation/icons/gitlab.svg "GitLab"){height=16px} GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html) (GLFM).

### Eclipse

Configure [![Eclipse](./documentation/icons/eclipse.png "Eclipse"){height=16px} Eclipse](https://eclipse.org/downloads/packages) to use this external directory:

 * In Package Explorer right-click and select `New` > `Project…`
 * Select `Wizards` > `General` > `Project` > `Next`:
   * `Project name`: `development-production-line-docker-demo`
   * Uncheck `Use default location`
   * `Location`: `%PROJECT_HOME%\development-production-line-docker-demo`
   * `Working sets` > `New…`
     * `Working set name`: `dpl-docker-demo` and click `Finish`
   * Click `Finish` again to create the project.

## WSL2 - Windows Subsystem for Linux

Based on [Windows Subsystem for Linux Documentation](https://learn.microsoft.com/en-us/windows/wsl) and enhanced with some code to verify stuff.

Open an *elevated* [PowerShell](https://learn.microsoft.com/en-us/powershell) (![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS) or [Windows Command Prompt](https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands) (![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD) in **administrator** mode by right-clicking and selecting `Run as administrator`.

To [Install Linux on Windows with WSL](https://learn.microsoft.com/en-us/windows/wsl/install) without looking at the gory details jump to step 6.

Otherwise, follow the [Manual installation steps for older versions of WSL](https://learn.microsoft.com/en-us/windows/wsl/install-manual) below from the start.

### Step 1

Enable the Windows Subsystem for Linux (see also [Enable or Disable Windows Features Using DISM](https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/enable-or-disable-windows-features-using-dism)):

```
dism /Online /Enable-Feature /FeatureName:Microsoft-Windows-Subsystem-Linux /All /NoRestart
dism /Online /Get-FeatureInfo /FeatureName:Microsoft-Windows-Subsystem-Linux
```

**Alternative way**: Check the feature at ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Turn Windows features on or off` > `Windows Subsystem for Linux`.

### Step 2

To get the current Windows version, to see if the requirements for WSL 2 are met, use:

```
winver
systeminfo | findstr Version
```

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
ver
```

### Step 3

Enable Virtual Machine feature:

```
dism /Online /Enable-Feature /FeatureName:VirtualMachinePlatform /All /NoRestart
dism /Online /Get-FeatureInfo /FeatureName:VirtualMachinePlatform
```

**Alternative way**: Check the feature at ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Turn Windows features on or off` > `Virtual Machine Platform`.

Restart for both changes from step 1 and 3 to take effect.

Verify at ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Task Manager` > `Performance` > `CPU` > `Virtualization`: `Enabled`.

### Step 4

Use `systeminfo` to find out the `System Type`:

```
systeminfo | findstr System
systeminfo | findstr /C:System
```

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
systeminfo | find "System"
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS only (because of error *FIND: Parameter format not correct*):

```
systeminfo | find """System"""
systeminfo | find `"System`"
systeminfo | find --% "System"
```

Download the Linux kernel update package ([Release Notes](https://learn.microsoft.com/en-us/windows/wsl/release-notes)) and run:

1. [![WSL2](./documentation/icons/wsl2.png "WSL2"){height=16px} WSL2 Linux kernel update X64 package](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
2. [![WSL2](./documentation/icons/wsl2.png "WSL2"){height=16px} WSL2 Linux kernel update ARM64 package](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_arm64.msi)

### Step 5

Set WSL 2 as your default version or for an installed distribution specific (that last one of course after installing it first; see step 6):

```
wsl --set-default-version 2
wsl --set-version Ubuntu-22.04 2
wsl --status
```

### Step 6

Install the default distribution, when you skipped the prior manual steps 1-5, otherwise, this command might just show the WSL help instead:

```
wsl --install
```

Or install your ![Linux](./documentation/icons/linux.png "Linux"){height=16px} Linux distribution of choice, for example ![Ubuntu](./documentation/icons/ubuntu.png "Ubuntu"){height=16px} Ubuntu:

```
wsl --list --online
wsl --install -d Ubuntu-22.04
```

During installation:

```
Installing, this may take a few minutes…
Please create a default UNIX user account. The username does not need to match your Windows username.
For more information visit: https://aka.ms/wslusers
Enter new UNIX username:
```

**Note**: https://aka.ms/wslusers is not redirecting properly to [Set up your Linux username and password](https://learn.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password).

Verify the installation:

```
wsl --status
wsl --list
wsl --list --verbose
wsl -l -v
```

To update WSL, because the prior installed `msi` isn't updated as frequently as the [WSL in the Microsoft Store](https://learn.microsoft.com/en-us/windows/wsl/compare-versions) (in an older WSL version for example `wsl --version` and `wsl --install` were not working as intended):

```
wsl --update
wsl --version
```

To start the default or a specific distribution:

```
wsl
wsl -d Ubuntu-22.04
wsl -d Ubuntu-22.04 --user <unix-username>
```

**Tip**: In case of ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash use `winpty wsl`.

Verify Ubuntu with:

```
whoami
lsb_release -a
cat /etc/os-release
ll /mnt
ll /mnt/c
exit
```

Stop a specific distribution with:

```
wsl -l -v
wsl --terminate Ubuntu-22.04
wsl -l -v
```

Or stop them all with:

```
wsl --shutdown
```

See also [Basic commands for WSL](https://learn.microsoft.com/en-us/windows/wsl/basic-commands).

```
wsl -l -v
wsl date
wsl -l -v
wsl --shutdown
```

## Docker Desktop on Windows

To [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install) download [![Docker Desktop](./documentation/icons/docker-desktop.png "Docker Desktop"){height=16px} Docker Desktop Installer.exe](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe) ([Release Notes](https://docs.docker.com/desktop/release-notes)).

**Known issues**:

 * Versions 4.23.0 - 4.24.2 `Quit Docker Desktop` hangs, causing:
   * ![Docker Failure](./documentation/icons/docker-failure.png "Docker Failure"){height=16px} **Docker failed to initialize** *Docker Desktop is shutting down* when trying to start ![Docker Desktop](./documentation/icons/docker-desktop.png "Docker Desktop"){height=16px} again.
   * ![Docker App](./documentation/icons/wfc.png "Docker App"){height=16px} *This app is preventing shutdown* on ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Power` > `Shut down`.

Verify the cryptographic hash with:

```
certutil -hashfile "Docker Desktop Installer.exe" SHA256
```

The ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash counterpart is:

```
sha256sum Docker\ Desktop\ Installer.exe
```

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
start /w "" "Docker Desktop Installer.exe" install
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS only:

```
Start-Process 'Docker Desktop Installer.exe' -Wait install
```

During installation select the default setting: `Use WSl 2 instead of Hyper-V (recommended)`. The installation logging can be found at:

![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD only:

```
type %LOCALAPPDATA%\Docker\install-log.txt
type %PROGRAMDATA%\DockerDesktop\install-log-admin.txt
```

![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS only:

```
type $env:LOCALAPPDATA\Docker\install-log.txt
type $env:PROGRAMDATA\DockerDesktop\install-log-admin.txt
```

**Important**: Make sure to close other applications, because of `docker-users` membership the installation might ask to log out. But if you change your mind and choose `Cancel` in case of the Windows Pop-up about still open applications with the option to go back and save your work, then for example ![Notepad++](./documentation/icons/notepad.png "Notepad++"){height=16px} `Notepad++` will still be open, but all prior open files are gone from the `Document List`.

When the `Installation succeeded` then click `Close and log out`.

After restart run ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Docker Desktop` and `Accept` the `Docker Subscription Service Agreement` after reading.

Use ![Windows Key](./documentation/icons/windows-key.png "Windows Key"){height=16px} + `I` > `Select which icons appear on the taskbar` to add `Docker Desktop`. Use the ![Docker Whale](./documentation/icons/docker-whale.png "Docker Whale"){height=16px} in the system tray to `Go to the Dashboard` > ![Docker Settings](./documentation/icons/docker-settings.png "Docker Settings"){height=16px} `Settings` and take a look at the current settings (for example `General` > `Send usage statistics`) followed by `Apply & retart` Docker Desktop, `Cancel` or ![Close Settings](./documentation/icons/docker-close.png "Close Settings"){height=16px} to return to the Dashboard.

Check available groups and the users in the `docker-users` group and add if necessary:

```
net
net localgroup
net users
net localgroup docker-users
net localgroup docker-users <windows-user> /add
```

**Alternative way**: ![Start](./documentation/icons/windows.png "Start"){height=16px} > `Computer Management` > `System Tools` > `Local Users and Groups` > `Groups` > `docker-users` > `Add…` > `Enter the object names to select` > `Check Names` > `OK`.

[Docker Desktop WSL 2 backend on Windows](https://docs.docker.com/desktop/wsl) installs two special-purpose internal *Linux distros* `docker-desktop` and `docker-desktop-data`. The first (`docker-desktop`) is used to run the Docker engine (`dockerd`) while the second (`docker-desktop-data`) stores containers and images. Neither can be used for general development.

```
wsl -l -v
```

## Apache Lucene MMapDirectory Tuning

When using [![Lucene](./documentation/icons/apache-lucene.png "Lucene"){height=16px} Lucene](https://lucene.apache.org) (for example with [![Elastic](./documentation/icons/elastic.png "Elastic"){height=16px} Elastic](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)) configure a higher value for `vm.max_map_count` (see also [![PDF](./documentation/icons/pdf.svg "PDF"){height=16px} The future of Lucene's MMapDirectory: Why use it and what's coming with Java 16 and later?](https://2021.berlinbuzzwords.de/sites/berlinbuzzwords.de/files/2021-06/The%20future%20of%20Lucene%27s%20MMapDirectory.pdf) or watch [![YouTube](./documentation/icons/youtube.png "YouTube"){height=16px}](https://www.youtube.com/watch?v=hgF0jNxKrrg)).

### Option 1 - Globally change every WSL distribution

Edit `%USERPROFILE%\.wslconfig`:

```
[wsl2]
kernelCommandLine = "sysctl.vm.max_map_count=262144"
```

Use the ![Docker Whale](./documentation/icons/docker-whale.png "Docker Whale"){height=16px} in the system tray to `Quit Docker Desktop` otherwise it will interfere WSL shutdown with the following Pop-up ![Docker Failure](./documentation/icons/docker-failure.png "Docker Failure"){height=16px} *Docker Desktop - WSL distro terminated abruptly*, which might cause the `.wslconfig` change not being picked up:

```
wsl --shutdown
```

### Option 2 - Specific WSL instance `docker-desktop` only

Modify the `/etc/sysctl.conf` within the `docker-desktop` WSL instance:

```
wsl -d docker-desktop
ls -l /etc/sysctl.conf
vi /etc/sysctl.conf
```

**Tip**: In case of ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash use `winpty wsl -d docker-desktop`.

Add `vm.max_map_count` entry to `/etc/sysctl.conf`:

```
# content of this file will override /etc/sysctl.d/*
vm.max_map_count = 262144
```

After saving this change with `:wq` activate it:

```
sysctl vm.max_map_count
sysctl -p
sysctl -qp
sysctl vm.max_map_count
exit
```

To make sure this settings is still active after WSL has been shutdown:

```
wsl -d docker-desktop
ls -l /etc/wsl.conf
vi /etc/wsl.conf
```

Add `boot` entry to `/etc/wsl.conf`:

```
[boot]
command = "sysctl -qp"

[automount]
 …
```

## Jenkins

### Controller

Use [![Docker Compose](./documentation/icons/docker-docs.png "Docker Compose"){height=16px} Docker Compose](https://docs.docker.com/compose) `docker-compose.yml` and `.env` files to create a [![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px} Jenkins Controller](https://github.com/jenkinsci/docker) service (at the time of writing based on `2.426.2 LTS`).

With ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash in `%PROJECT_HOME%/development-production-line-docker-demo` start the ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px} Jenkins Controller at [localhost:8080](http://localhost:8080):

```
winpty docker compose up -d
docker ps
```

**Note**: In case of PowerShell (![PowerShell](./documentation/icons/pwsh.png "PowerShell"){height=16px} PS) or Windows Command Prompt (![CMD](./documentation/icons/cmd.png "CMD"){height=16px} CMD) drop `winpty`.

### Volume

To get hold of the generated `password` from the container logs or `cat` the in the browser mentioned file:

```
docker logs jenkins 2>&1 | grep 'a password generated' -A5
docker logs jenkins |& grep 'a password generated' -A5

winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"

winpty docker exec -it jenkins bash
user#CONTAINER_ID:/$ cat /var/jenkins_home/secrets/initialAdminPassword
user#CONTAINER_ID:/$ exit
```

**Note**: Since the *pipeline* sends `stdout` via a pipe `|` as `stdin` to `grep` and `docker logs` shows `stdout` and `stderr` use `2>&1 |` or shortcut `|&`.

Stop and start the **service** `jenkins-controller` and fetch the `initialAdminPassword` again:

```
winpty docker compose stop jenkins-controller
docker ps
winpty docker compose start jenkins-controller
docker ps
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```

Stop and start the **container** `jenkins` and fetch the `initialAdminPassword` again:

```
docker stop jenkins
docker ps
docker start jenkins
docker ps
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```

The generated `password` is still the same.


Now stop the **service** `jenkins-controller`, while also removing the **container**, and then create and start:

```
winpty docker compose down
docker ps
winpty docker compose up -d
docker ps
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```

The generated `password` is different because the new container is associated with a new volume.

#### Option 1 - Volume Bind Mount

With the following volume bind mount:

```
services:
  jenkins-controller:
    image: jenkins/jenkins:${JENKINS_VERSION}
    volumes:
      - ./volumes/jenkins/volume-bind-mount:/var/jenkins_home
```

Create a volume in `./volumes/jenkins/volume-bind-mount`:

```
winpty docker compose up -d
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
winpty docker compose down

winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
cat ./volumes/jenkins/volume-bind-mount/secrets/initialAdminPassword
```

#### Option 2 - Named Volume

With the following named volume:

```
services:
  jenkins-controller:
    image: jenkins/jenkins:${JENKINS_VERSION}
    volumes:
      - jenkins-data:/var/jenkins_home

volumes:
  jenkins-data:
```

Create a volume in Docker Desktop `/var/lib/docker/volumes` (see File Explorer `\\wsl$\docker-desktop-data\data\docker\volumes`):

```
winpty docker compose up -d
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
docker volume ls -f dangling=false
docker volume inspect development-production-line-docker-demo_jenkins-data
winpty docker compose down
```


#### Option 3 - Named Volume Bind Mount

With the following named volume bind mount:

```
services:
  jenkins-controller:
    image: jenkins/jenkins:${JENKINS_VERSION}
    volumes:
      - jenkins-local-data:/var/jenkins_home

volumes:
  jenkins-local-data:
    driver: local
    driver_opts:
      type: none
      o: bind
      device: ./volumes/jenkins/named-volume-bind-mount
```

Create a volume in `./volumes/jenkins/named-volume-bind-mount`:

```
winpty docker compose up -d
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
docker volume ls -f dangling=false
docker volume inspect development-production-line-docker-demo_jenkins-local-data
winpty docker compose down

cat ./volumes/jenkins/named-volume-bind-mount/secrets/initialAdminPassword
```

#### Volume User Permission

When the volume is created with user `root` and at a later stage the (default) container user `jenkins` is used, then the service will fail to start with:

> Can not write to /var/jenkins_home/copy_reference_file.log. Wrong volume permissions?
> touch: cannot touch '/var/jenkins_home/copy_reference_file.log': Permission denied

Switch back to `root` again and after restart:

```
winpty docker exec -it jenkins bash -c "ls -l /var/jenkins_home"
winpty docker exec -it jenkins bash -c "chown -R jenkins:jenkins /var/jenkins_home"
```

Now we should be able to start the service with user `jenkins`, but what other permissions might be wrong?

```
winpty docker exec -it jenkins bash -c "ls -l /usr/share/jenkins/ref"
```

Better start anew and thus remove the container inclusive the volume:

```
winpty docker compose down -v
```

In case of (named) volume bind mount also remove the local volume folder:

```
rm -r ./volumes/jenkins/named-volume-bind-mount
rm -r ./volumes/jenkins/volume-bind-mount
```

Finally clean up old anonymous volumes, that were created before the volume was mapped:

```
docker volume prune
```

### Unlock Jenkins

With the volume in place it is time to `Unlock Jenkins` with the latest generated `password`:

```
winpty docker exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```

Next `Select plugins to install` because the `suggested plugins` are [preselected](https://github.com/jenkinsci/jenkins/blob/master/core/src/main/resources/jenkins/install/platform-plugins.json) anyway. Lets start with `None` and click `Install`.

After `Create First Admin User` click `Save and Continue`.

Keep the `Instance Configuration` at `http://localhost:8080/` and click `Save and Finish`.

### Jenkins Configuration as Code Plugin

The [Jenkins Configuration as Code (a.k.a. JCasC) Plugin](https://github.com/jenkinsci/configuration-as-code-plugin) is an *opinionated* way to configure Jenkins based on human-readable declarative configuration files (see also [demos](https://github.com/jenkinsci/configuration-as-code-plugin/tree/master/demos)).

Install with the [Plugin Installation Manager Tool for Jenkins](https://github.com/jenkinsci/plugin-installation-manager-tool):

```
winpty docker exec -it jenkins bash -c "jenkins-plugin-cli --list"
winpty docker exec -it jenkins bash -c "jenkins-plugin-cli --plugins configuration-as-code:1763.vb_fe9c1b_83f7b"
```

**Note**: The call to `list` is done to get an idea of the performance of the [Jenkins Update Site](https://updates.jenkins.io) or [mirrors](https://get.jenkins.io/plugins/configuration-as-code/1763.vb_fe9c1b_83f7b/configuration-as-code.hpi?mirrorstats), which sometimes results in:

> INFO: I/O exception (org.apache.http.NoHttpResponseException) caught when processing request to {s}->https://updates.jenkins.io:443: The target server failed to respond.

**Alternative way**: When the performance is really slow, try the UI ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px}  `Jenkins` > `Dashboard` > `Manage Jenkins` > `System Configuration` > `Plugins` > `Available plugins`. In `Search available plugins` type `Configuration as Code`, tick the appropriate box and `Install` at [localhost:8080/manage/pluginManager/available](http://localhost:8080/manage/pluginManager/available).

After downloading with `jenkins-plugin-cli` a restart of Jenkins at [localhost:8080/safeRestart](http://localhost:8080/safeRestart) is required.

Alas, the list of plugins shown at [localhost:8080/manage/pluginManager/installed](http://localhost:8080/manage/pluginManager/installed) doesn't always match with:

```
winpty docker exec -it jenkins bash -c "jenkins-plugin-cli --list --output txt"
```

Therefore select ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px}  `Jenkins` > `Dashboard` > `Manage Jenkins` > `Tools and Actions` > `Script Console` and `Run` at [localhost:8080/manage/script](http://localhost:8080/manage/script):

```
def plugins = jenkins.model.Jenkins.instance.getPluginManager().getPlugins()
plugins.each {println "${it.getShortName()}:${it.getVersion()}"}
```

With that info create the initial project setup of `./jenkins-controller/plugins.txt`:

```
snakeyaml-api:2.2-111.vc6598e30cc65
json-api:20231013-3.v20f3c247f2fe
commons-lang3-api:3.13.0-62.v7d18e55f51e2
commons-text-api:1.11.0-95.v22a_d30ee5d36
structs:325.vcb_307d2a_2782
scm-api:683.vb_16722fb_b_80b_
caffeine-api:3.1.8-133.v17b_1ff2e0599
script-security:1294.v99333c047434
bouncycastle-api:2.30.1.77-225.v26ea_c9455fd9
instance-identity:185.v303dc7c645f9
javax-activation-api:1.2.0-6
javax-mail-api:1.6.2-9
workflow-step-api:639.v6eca_cd8c04a_a_
workflow-api:1283.v99c10937efcb_
workflow-support:865.v43e78cc44e0d
plugin-util-api:3.8.0
font-awesome-api:6.5.1-1
bootstrap5-api:5.3.2-3
antisamy-markup-formatter:162.v0e6ec0fcfcf6
prism-api:1.29.0-10
configuration-as-code:1763.vb_fe9c1b_83f7b
```

To check for updates:

```
sort -o ./jenkins-controller/plugins.txt ./jenkins-controller/plugins.txt

docker cp ./jenkins-controller/plugins.txt jenkins:/usr/share/jenkins/ref/plugins.txt

winpty docker exec -it jenkins bash -c "jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt --available-updates"
winpty docker exec -it jenkins bash -c "jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt --available-updates --output txt"
winpty -Xplain -Xallow-non-tty docker exec -it jenkins bash -c "jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt --available-updates --output txt" > ./jenkins-controller/plugins-updates.txt

diff ./jenkins-controller/plugins.txt ./jenkins-controller/plugins-updates.txt
```

**Note**: Use `-Xallow-non-tty` to fix `winpty` error *stdout is not a tty* and use `-Xplain` to suppress the ANSI escape sequences.

### Dockerfile Custom Controller

Create a custom Docker image with the current initial configuration and `plugins.txt`.

To get that configuration select ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px}  `Jenkins` > `Dashboard` > `Manage Jenkins` > `System Configuration` > `Configuration as Code` and `View Configuration` at [localhost:8080/manage/configuration-as-code](http://localhost:8080/manage/configuration-as-code).

**Warning**: *Export is not intended to offer a directly usable `jenkins.yaml` configuration. It can be used for inspiration writing your own, be aware export can be partial, or fail for some components.*

Create non-overlapping YAML files in `./jenkins-controller/casc`:

 * [System](http://localhost:8080/manage/configure) in `system.yaml`
 * [Tools](http://localhost:8080/manage/configureTools) in `tools.yaml`
 * [Plugins](http://localhost:8080/manage/pluginManager/advanced) in `plugins.yaml`
 * [Appearance](http://localhost:8080/manage/appearance) in `appearance.yaml`
 * [Security](http://localhost:8080/manage/configureSecurity) in `security.yaml`
 * [Users](http://localhost:8080/manage/securityRealm) in `users.yaml` and more specific [admin](http://localhost:8080/user/admin/configure)
 * Unkown in `other.yaml`

With the following environment variables:

 * `JENKINS_ADMIN_USERNAME`: The username of the administrator. Default: `admin`.
 * `JENKINS_ADMIN_NAME`: The name of the `admin` user. Default: `Jenkins Administrator`.
 * `JENKINS_ADMIN_PASSWORD`: The `password` for the administrator. Default: `butler`.
 * `JENKINS_ADMIN_MAIL`: The `e-mail address` for the administrator. Default: `address not configured yet <nobody@nowhere>`.
 * `JENKINS_LOCATION_URL`: The `Jenkins URL`. Default: `http://localhost:8080/`.

Testing the `Dockerfile` with latest version:

```
winpty docker build -t jenkins/jenkins:tst ./jenkins-controller
docker images

winpty docker run -d -p 127.0.0.1:8000:8080 --name jenkins-tst jenkins/jenkins:tst
docker ps
```

Verify Jenkins version at [localhost:8000](http://localhost:8000).

Now rebuild with a LTS version:

```
winpty docker build --build-arg VERSION=2.426.2-lts-jdk17 -t jenkins/jenkins:tst ./jenkins-controller
docker images

docker stop jenkins-tst
docker ps -a
docker rm jenkins-tst
winpty docker run -d -p 127.0.0.1:8000:8080 -e JENKINS_LOCATION_URL=http://localhost:8000/ --name jenkins-tst jenkins/jenkins:tst
```

**Tip**: Run the base image at a different port without volume mapping to experiment with plugins and configs:

```
winpty docker run -d -p 127.0.0.1:8081:8080 --name jenkins-lts jenkins/jenkins:2.426.2-lts-jdk17
docker logs jenkins-lts |& grep 'a password generated' -A5

docker stop jenkins-lts
docker rm jenkins-lts
```

After editing the `docker-compose.yml` to `build` the image:

```
services:
  jenkins-controller:
    image: jenkins/jenkins:${JENKINS_IMAGE_TAG}
    build:
      context: ./jenkins-controller
      args:
        VERSION: ${JENKINS_VERSION}
    container_name: jenkins
```

with `.env` addition of `JENKINS_IMAGE_TAG=dpl`, switch to usage of this custom Jenkins Controller with one of the following commands:

```
winpty docker compose up --build -d
winpty docker compose up --build --force-recreate -d
```

**Note**: In case of error *io.jenkins.plugins.casc.ConfiguratorException: Invalid configuration: '/var/jenkins_home/casc.d' isn't a valid path.* remove the volume and bind mount first:

```
winpty docker compose down -v
rm -rf volumes/jenkins/var/
```

Just creating it in the project `volumes` location with:

```
mkdir volumes/jenkins/var/jenkins_home/casc.d
```

will work for getting Jenkins up and running, but the files in `/var/jenkins_home/casc.d` are not displayed, because it had the wrong owner.

### SSH Build Agent

The Jenkins architecture is designed for distributed build environments. It allows us to use different environments for each build project balancing the workload among multiple [agents](https://www.jenkins.io/doc/book/using/using-agents) running jobs in parallel.

The [SSH Build Agents Plugin](https://github.com/jenkinsci/ssh-agents-plugin) (formerly known as SSH Slaves Plugin) provides a means to launch agents via SSH.

Modify the existing JCasC project setup of `./jenkins-controller/plugins.txt` to add or update the following entries:

```
gson-api:2.10.1-3.vb_25b_599b_e4f8
trilead-api:2.133.vfb_8a_7b_9c5dd1
structs:325.vcb_307d2a_2782
credentials:1311.vcf0a_900b_37c2
ssh-credentials:308.ve4497b_ccd8f4
ssh-slaves:2.947.v64ee6b_f87b_c1
```

Rebuild the Jenkins Controller image:

```
winpty docker compose up --build -d
docker ps
```

#### Secure Shell (SSH) Key

To allow the controller to access the agent via SSH, generate a SSH key pair `jenkins-agent-rsa` with or without a passphrase:

```
ssh-keygen -C jenkins -f jenkins-agent-rsa
ssh-keygen -C jenkins -f jenkins-agent-rsa -N 'password'
ssh-keygen -C jenkins -f jenkins-agent-rsa -N ''
```

Verify the password or the lack off:

```
ssh-keygen -y -f jenkins-agent-rsa
ssh-keygen -y -f jenkins-agent-rsa -P 'password'
```

Compare fingerprints:

```
ssh-keygen -lf jenkins-agent-rsa
ssh-keygen -lf jenkins-agent-rsa.pub
```

Select ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px}  `Jenkins` > `Dashboard` > `Manage Jenkins` > `Security` > `Credentials`. Under `Stores scoped to Jenkins` at [localhost:8080/manage/credentials](http://localhost:8080/manage/credentials) click `System` > `Global credentials (unrestricted)` > `Add Credentials`:

 * `Kind`: `SSH Username with private key`
 * `Scope`: `System (Jenkins and nodes only)`
 * `ID`: `jenkins-agent-credentials`
 * `Description`: `Jenkins Agent SSH Key`
 * `Username`: `jenkins`
 * `Private Key`:
  * `Enter directly` > `Key` > `Add` > `Enter New Secret Below`:
   * `-----BEGIN OPENSSH PRIVATE KEY-----`
   * `     …`
   * `-----END OPENSSH PRIVATE KEY-----`
 * `Passphrase`: empty or `password`
 * Finish with `Create`.

Use [![Docker Compose](./documentation/icons/docker-docs.png "Docker Compose"){height=16px} Docker Compose](https://docs.docker.com/compose) `docker-compose.yml` and `.env` files to create a [![SSH Agent](./documentation/icons/jenkins.svg "SSH Agent"){height=16px} SSH Agent](https://github.com/jenkinsci/docker-ssh-agent) service (at the time of writing based on `5.22.0`).

With ![Git Bash](./documentation/icons/git.png "Git Bash"){height=16px} Git Bash in `%PROJECT_HOME%/development-production-line-docker-demo` start the ![SSH Agent](./documentation/icons/jenkins.svg "SSH Agent"){height=16px} SSH Agent named Jeeves together with the Jenkins Controller:

Verify `JENKINS_AGENT_SSH_PUBKEY` with:

```
winpty docker compose up -d
docker ps

winpty docker exec -it jeeves bash -c "export"
winpty docker exec -it jeeves bash -c "cat /home/jenkins/.ssh/authorized_keys"

winpty docker exec -it jeeves bash -c "//opt/java/openjdk/bin/java --version"
winpty docker exec -it jeeves bash -c "\/opt/java/openjdk/bin/java --version"
winpty docker exec -it jeeves bash -c " /opt/java/openjdk/bin/java --version"
```

**Note**: With 3 workarounds that prevent conversion to a Windows path for `winpty docker exec -it jeeves bash -c "/opt/java/openjdk/bin/java --version"`, which causes the following error *No such file or directory*.

#### Node Agent Jeeves

Select ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px}  `Jenkins` > `Dashboard` > `Manage Jenkins` > `System Configuration` > `Nodes` and `New Node` at [localhost:8080/manage/computer](http://localhost:8080/manage/computer).

Enter `Node name`: `Jeeves` of `Type`: `Permanent Agent` and click `Create`:
 * `Description`: `Jenkins Maven build agent.`
 * `Number of executors`: `3`
 * `Remote root directory`: `/home/jenkins/agent`
 * `Labels`: `maven`
 * `Usage`: `Use this node as much as possible`
 * `Launch method`: `Launch agents via SSH`
  * `Host`: `jeeves` **Note**: Each container can reach the others by using their container names as hostnames.
  * `Credentials`: `jenkins (Jenkins Agent SSH Key)`
  * `Host Key Verification Strategy`: `Non verifying Verification Strategy` (or `Manually trusted key Verification Strategy` without `Require manual verification of initial connection`
  * `Advanced`
   * `JavaPath`: `/opt/java/openjdk/bin/java`
 * `Availability`: `Keep this agent online as much as possible`
 * Finish with `Save`.

Select ![Jenkins Controller](./documentation/icons/jenkins.svg "Jenkins Controller"){height=16px}  `Jenkins` > `Dashboard` > `Manage Jenkins` > `System Configuration` > `Nodes` > `Jeeves` > `Log` at [localhost:8080/computer/Jeeves/log](http://localhost:8080/computer/Jeeves/log) and verify the logging that *Agent successfully connected and online*.

Stop the `ssh-agent` service with:

```
winpty docker compose down ssh-agent
docker ps
```

Verify at [localhost:8080/computer/Jeeves/log](http://localhost:8080/computer/Jeeves/log) that after the last retry it will go offline and after awhile Jenkins will try to launch it again.

Restart the `ssh-agent` service again:

```
winpty docker compose up ssh-agent -d
docker ps
```

Alas, after the restart the Jeeves SHH Host Key is changed and we need to [![Trust SSH Host Key](./documentation/icons/jenkins-trust-ssh-host-key.svg "Trust SSH Host Key"){height=16px} Trust SSH Host Key](http://localhost:8080/computer/Jeeves) it again or switch from `Manually trusted key Verification Strategy` to `Non verifying Verification Strategy`. In both cases a `Relaunch agent` might be needed. As an alternative switch to `Known hosts file Verification Strategy` with the use of a custom Docker image.

**Tip**: To verify the new fingerprint run:

```
winpty docker exec -it jeeves bash -c "ssh-keygen -lf /etc/ssh/ssh_host_ed25519_key -E md5"
```

### Dockerfile Custom Agent
